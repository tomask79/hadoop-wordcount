package myhadoop.avg;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WeatherAnalysisCombinerAvg extends Reducer<Text, Text, Text, Text>{
	
	int count = 0;
	int sum = 0;
 
    @Override
    protected void reduce(Text key, Iterable<Text> values,
                          Context context)
            throws IOException, InterruptedException {
    
    	for (Text text: values) {
    		count++;
    		final WeatherItem item = new WeatherItem(text);
    		sum = sum + item.getTemperature();
    	}
    	
    	System.out.println("Pushing from combiner [sum]: "+sum);
    	System.out.println("Pushing from combiner [cnt]: "+count);
    	context.write(new Text("combined"), new Text(sum+":"+count));
    }
    
    
}
