package myhadoop.avg;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class WeatherAnalysisMapperAvg extends Mapper<LongWritable, Text, Text, Text> {
	@Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, 
    InterruptedException {
    	String line = value.toString();
        context.write(new Text("mapOutput"), new Text(line));
    }
}
