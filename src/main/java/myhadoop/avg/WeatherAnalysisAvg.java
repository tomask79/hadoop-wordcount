package myhadoop.avg;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class WeatherAnalysisAvg {
    public static void main(String[] args) throws Exception {
    	Configuration conf = new Configuration();
        
    	Job job = new Job(conf, "weatherAnalysisAvg");
    	       
    	job.setMapperClass(WeatherAnalysisMapperAvg.class);
    	job.setReducerClass(WeatherAnalysisReducerAvg.class);
    	job.setCombinerClass(WeatherAnalysisCombinerAvg.class);
    	
    	job.getConfiguration().set("inputPath", args[0]);
    	FileInputFormat.setInputPaths(job, new Path(args[0]));
    	FileOutputFormat.setOutputPath(job, new Path(args[1]));
    	
    	
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        
        job.setNumReduceTasks(1);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
 
        job.waitForCompletion(true);
    }
}
