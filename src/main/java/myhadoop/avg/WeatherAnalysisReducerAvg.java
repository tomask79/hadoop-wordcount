package myhadoop.avg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WeatherAnalysisReducerAvg extends Reducer<Text, Text, Text, Text>{
	
	int count = 0;
	float sum = 0f;
	float avg = 0f;
 
	
    @Override
    protected void reduce(Text key, Iterable<Text> values,
                          Context context)
            throws IOException, InterruptedException {
    	for (Text text: values) {
    		String arr[] = text.toString().split(":");
    		System.out.println("Reducer :"+arr[0]);
    		count = count + Integer.parseInt(arr[1]);
    		sum = sum + Integer.parseInt(arr[0]);
    	}
    	
    	avg = sum / count;
    	System.out.println("Counted avg: "+avg);
    }
    
    protected void cleanup(Context context) 
    		throws IOException, InterruptedException {
    	String inputPath = context.getConfiguration().get("inputPath");
    	Path path = new Path(inputPath);
    	
    	FileSystem fs = FileSystem.get(new Configuration());
        BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(path)));
        String line;
        while ((line = br.readLine()) != null){
            final WeatherItem item = new WeatherItem(line);
            if (item.getTemperature() > avg) {
            	context.write(new Text(item.getCity()), 
            				  new Text(item.getTemperature()+""));
            }
        }
    }
}