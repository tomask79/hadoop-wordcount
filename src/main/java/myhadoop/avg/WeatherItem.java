package myhadoop.avg;

import org.apache.hadoop.io.Text;

public class WeatherItem {
	private String city;
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getTemperature() {
		return temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	private Integer temperature;
	private Integer year;
	
	public WeatherItem(Text text) {
	 	this(text.toString());
	}
	
	public WeatherItem(String line) {
		String splitarray[] = line.split(",");
		this.city = splitarray[0].trim();
        this.temperature = Integer.valueOf(splitarray[1].trim());
        this.year = Integer.valueOf(splitarray[2].trim());
	}
}
