package myhadoop.joins.reduce;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class StudentsOutputReducer extends Reducer<Text, Text, Text, Text> {
	 
    public void reduce(Text key, Iterable<Text> values, Context context) 
      throws IOException, InterruptedException { 
    	String result = "";
    	for (Text text: values) {
    		result = result + text + ",";
    	}
    	
    	context.write(key, new Text(result));
    }
}
