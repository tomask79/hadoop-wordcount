package myhadoop.joins.reduce;

import java.io.IOException;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class StudentsMapper extends Mapper<LongWritable, Text, Text, Text>{
	
	public void map(LongWritable key, Text value, Context context) 
			throws IOException, InterruptedException {
		String line = value.toString();
        String splitarray[] = line.split(",");
        String studentName = splitarray[0].trim();
        String city = splitarray[1].trim();
        
        context.write(new Text(studentName), new Text(city));
	}
}
