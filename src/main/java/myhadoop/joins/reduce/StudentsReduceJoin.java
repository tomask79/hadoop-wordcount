package myhadoop.joins.reduce;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class StudentsReduceJoin {
    public static void main(String[] args) throws Exception {
    	//get the configuration parameters and assigns a job name
    	Configuration conf = new Configuration();
        
    	Job job = new Job(conf, "students");

        //setting key value types for mapper and reducer outputs
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        //specifying the custom reducer class
        job.setReducerClass(StudentsOutputReducer.class);

        //Specifying the input directories(@ runtime) and Mappers independently for inputs from multiple sources
        MultipleInputs.addInputPath(job, new Path(args[0]), 
     		   TextInputFormat.class, StudentsMapper.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), 
     		   TextInputFormat.class, StudentsResultsMapper.class);
       
        //Specifying the output directory @ runtime
        FileOutputFormat.setOutputPath(job, new Path(args[2]));

        job.waitForCompletion(true);
    }
}
