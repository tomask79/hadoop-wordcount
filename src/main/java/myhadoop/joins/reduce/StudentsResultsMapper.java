package myhadoop.joins.reduce;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class StudentsResultsMapper extends Mapper<LongWritable, Text, Text, Text>{
	
	public void map(LongWritable key, Text value, Context context) 
			throws IOException, InterruptedException {
		String line = value.toString();
        String splitarray[] = line.split(",");
        String studentName = splitarray[0].trim();
        String testResult = splitarray[1].trim();
        
        context.write(new Text(studentName), new Text(testResult));
	}

}
