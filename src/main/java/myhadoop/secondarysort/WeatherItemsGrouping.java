package myhadoop.secondarysort;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class WeatherItemsGrouping extends WritableComparator {
	public WeatherItemsGrouping() {
	     super(WeatherItemsKey.class, true);
	}
	    
	@Override
	public int compare(WritableComparable tp1, WritableComparable tp2) {
		WeatherItemsKey weatherItemsPair1 = (WeatherItemsKey) tp1;
		WeatherItemsKey weatherItemsPair2 = (WeatherItemsKey) tp2;
	    return weatherItemsPair1.getRegion().compareTo(weatherItemsPair2.getRegion());
	}
}
