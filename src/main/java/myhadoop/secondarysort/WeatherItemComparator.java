package myhadoop.secondarysort;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class WeatherItemComparator extends WritableComparator {
	protected WeatherItemComparator() {
	    super(WeatherItemsKey.class, true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2) {
		WeatherItemsKey k1 = (WeatherItemsKey)w1;
	    WeatherItemsKey k2 = (WeatherItemsKey)w2;
	         
	    return k1.compareTo(k2);
	}
}
