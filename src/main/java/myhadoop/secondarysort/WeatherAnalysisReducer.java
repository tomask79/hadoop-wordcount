package myhadoop.secondarysort;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WeatherAnalysisReducer extends Reducer<WeatherItemsKey, Text, Text, Text> {
	 
    public void reduce(WeatherItemsKey key, Iterable<Text> values, Context context) 
      throws IOException, InterruptedException { 
    	String reducer = "";
    	for (Text text: values) {
    		reducer = reducer + text + " ";
    	}
    	System.out.println("Reducer key: "+key.getRegion());
    	System.out.println("Reducer line: "+reducer);
    	context.write(new Text(key.getRegion()), new Text(key.getTemperature()+""));
    }
}

