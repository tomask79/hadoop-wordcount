package myhadoop.secondarysort;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WeatherAnalysisMapper extends Mapper<LongWritable, Text, WeatherItemsKey, Text> {

    private WeatherItemsKey temperaturePair = new WeatherItemsKey();
    
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, 
    InterruptedException {
    	String line = value.toString();
        String splitarray[] = line.split(",");
        String region = splitarray[0].trim();
        Double temperature = Double.valueOf(splitarray[1].trim());
        String year = splitarray[2].trim();

        temperaturePair.setRegion(region);
        temperaturePair.setTemperature(temperature);
        context.write(temperaturePair, new Text(year));
    }
}