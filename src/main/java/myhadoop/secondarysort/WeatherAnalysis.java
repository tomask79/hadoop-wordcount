package myhadoop.secondarysort;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class WeatherAnalysis {
    public static void main(String[] args) throws Exception {
    	Configuration conf = new Configuration();
        
    	Job job = new Job(conf, "weatherAnalysis");
    	       
    	job.setOutputKeyClass(Text.class);
    	job.setOutputValueClass(Text.class);
    	           
    	job.setMapperClass(WeatherAnalysisMapper.class);
    	job.setReducerClass(WeatherAnalysisReducer.class);
    	           
    	job.setInputFormatClass(TextInputFormat.class);
    	job.setOutputFormatClass(TextOutputFormat.class);
    	
    	job.setMapOutputKeyClass(WeatherItemsKey.class);
    	job.setPartitionerClass(WeatherItemsPartitioner.class);
    	job.setGroupingComparatorClass(WeatherItemsGrouping.class);
    	job.setSortComparatorClass(WeatherItemComparator.class);
    	           
    	FileInputFormat.addInputPath(job, new Path(args[0]));
    	FileOutputFormat.setOutputPath(job, new Path(args[1]));
    	           
    	job.waitForCompletion(true);
    }
}

