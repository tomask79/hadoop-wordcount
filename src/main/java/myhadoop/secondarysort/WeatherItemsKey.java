package myhadoop.secondarysort;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class WeatherItemsKey implements WritableComparable {
    // Some data
    private String region;
    public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	private Double temperature;
    
    public void write(DataOutput out) throws IOException {
      out.writeUTF(region);
      out.writeDouble(temperature);
    }
    
    public void readFields(DataInput in) throws IOException {
    	region = in.readUTF();
        temperature = in.readDouble();
    }
    
    public int compareTo(Object o) {
    	WeatherItemsKey k1 = (WeatherItemsKey) o;
         
        int result = k1.region.compareTo(this.region);
        if(0 == result) {
            result = -1*k1.temperature.compareTo(this.temperature);
        }
        return result;
    }

    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + region.hashCode();
      return result;
    }
 }