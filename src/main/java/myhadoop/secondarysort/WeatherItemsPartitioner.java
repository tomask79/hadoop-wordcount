package myhadoop.secondarysort;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class WeatherItemsPartitioner extends Partitioner<WeatherItemsKey, Text>{
	@Override
	public int getPartition(WeatherItemsKey weatherItemKey, Text output, 
			int numPartitions) {
		return weatherItemKey.hashCode() % numPartitions;
	}
}
