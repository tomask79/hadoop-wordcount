package myhadoop.totalordersort;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WeatherAnalysisReducer extends Reducer<LongWritable, Text, Text, LongWritable> {
	 
    public void reduce(LongWritable key, Iterable<Text> values, Context context) 
      throws IOException, InterruptedException { 
    	for(Text val : values) {
            context.write(new Text(key + "," + val), null);
        }
    }
}

