package myhadoop.totalordersort;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WeatherAnalysisMapper extends Mapper<LongWritable, Text, LongWritable, Text> {
    
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, 
    InterruptedException {
    	String line = value.toString();
        String splitarray[] = line.split(",");
        String region = splitarray[0].trim();
        Long temperature = Long.valueOf(splitarray[1].trim());
        String year = splitarray[2].trim();

        context.write(new LongWritable(temperature), new Text("in "+region+" at "+year));
    }
}