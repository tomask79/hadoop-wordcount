package myhadoop.totalordersort;

import myhadoop.secondarysort.WeatherItemsKey;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.lib.InputSampler;
import org.apache.hadoop.mapred.lib.TotalOrderPartitioner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

/**
 * 
 * @author tomask79
 *
 */
public class WeatherAnalysis {
    public static void main(String[] args) throws Exception {
    	Configuration conf = new Configuration();
        
    	Job job = new Job(conf, "weatherAnalysisTotalOrderSort");
    	       
    	job.setMapperClass(WeatherAnalysisMapper.class);
    	job.setReducerClass(WeatherAnalysisReducer.class);
    	
    	job.setPartitionerClass(TotalOrderPartitioner.class);
    	
    	FileInputFormat.setInputPaths(job, new Path(args[0]));
    	FileOutputFormat.setOutputPath(job, new Path(args[1]));
    	
    	
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);
        job.setSortComparatorClass(WeatherItemComparator.class);
        
        job.setNumReduceTasks(1);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        
        // path to HDFS filesystem..;)
        Path inputDir = new Path("/user/totalorderpartition");
        Path partitionFile = new Path(inputDir, "partitioning");
        TotalOrderPartitioner.setPartitionFile(job.getConfiguration(),
                partitionFile);
 
        double pcnt = 10.0;
        int numSamples = 2;
        int maxSplits = 1;
        
        InputSampler.Sampler sampler = new InputSampler.RandomSampler(pcnt,
                numSamples, maxSplits);
        InputSampler.writePartitionFile(job, sampler);
    	job.waitForCompletion(true);
    }
}

