package myhadoop.totalordersort;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class WeatherItemComparator extends WritableComparator {
	protected WeatherItemComparator() {
	    super(LongWritable.class, true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2) {
		LongWritable k1 = (LongWritable)w1;
		LongWritable k2 = (LongWritable)w2;
	         
        if (k1.get() < k2.get()) {
            return 1;
        } else if(k1.get() > k2.get()) {
            return -1;
        } else {
            return 0;
        }
	}
}
