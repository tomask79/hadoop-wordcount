Algorithms using hadoop

1) Wordcount
Run it as: hadoop jar <compiled jar> myhadoop.test.App <hdfs path to input> <dir path to output dir>

2) Reduce side join example:
Run it as: hadoop jar <compiled jar> myhadoop.joins.reduce.StudentsReduceJoin <hdfs path to input1> <hdfs path to input2> <dir path to output dir>

3) MapReduce secondary sort example (chooses lowest temperature for region):
Run it as: hadoop jar <compiled jar> myhadoop.secondarysort.WeatherAnalysis <hdfs path to input1> <hdfs path to output dir>


```
Input example:
Brno,36,1996
Praha,35,1985
Brno,31,1985
Berlin,-18,1987
Paris,20,1987
Paris,21,1988
Brno,-18,1987
Praha,22,1943
Madrid,44,1944
Madrid,22,1933
Madrid,-17,1955
Berlin,-22,1966
Praha,-31,1976
```

```
Result:
Praha	-31.0 
Paris	20.0 
Madrid	-17.0 
Brno	-18.0 
Berlin	-22.0 
```

4) Total sort order example (sorts temperatures in descending order):

Run it as: 
hadoop jar <jar> myhadoop.totalordersort.WeatherAnalysis <hdfs path to txt> <hdfs output dir>

Input is the same as in secondary sort example, but result now is:
```
44,in Madrid at 1944
36,in Brno at 1996
35,in Praha at 1985
31,in Brno at 1985
22,in Madrid at 1933
22,in Praha at 1943
21,in Paris at 1988
20,in Paris at 1987
-17,in Madrid at 1955
-18,in Brno at 1987
-18,in Berlin at 1987
-22,in Berlin at 1966
-31,in Praha at 1976
```

5) Fifth example outputs only temperatures which are higher then AVG value counted from all values in the input dataset, task which user @MetadataQ gave me at twitter.

Input is still the same as in previous examples, but output is now:

```
Brno	36
Praha	35
Brno	31
Paris	20
Paris	21
Praha	22
Madrid	44
Madrid	22
```

Yes, freezing temperatures are gone, because they're lower then AVG.

Solution: I used combiners to minimalize the shuffled data, but final output is performed 
via just one reducer. Using more reducers for this task could be done in the separate MP task
with total sort order in the game. I will add this solution later.

Run example as:

hadoop jar <compiled jar> myhadoop.avg.WeatherAnalysisAvg <input dataset> <output hdfs dir>

enjoy and feel free to ask anything about hadoop and map reduce. Use my twitter https://twitter.com/TomasK79.